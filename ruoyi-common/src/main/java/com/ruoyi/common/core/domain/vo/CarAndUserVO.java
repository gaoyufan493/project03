package com.ruoyi.common.core.domain.vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.domain.entity.SysUser;


public class CarAndUserVO extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 车辆编号 */
    @Excel(name = "车辆编号")
    private Long carId;

    /** 车辆名称 */
    @Excel(name = "车辆名称")
    private String carName;

    /** 车辆颜色 */
    @Excel(name = "车辆颜色")
    private String color;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String num;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 创建人 */
    @Excel(name = "创建人")
    private Long userId;

    /** 司机_id */
    private Long driverId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public String getAnnex() {
        return annex;
    }

    public void setAnnex(String annex) {
        this.annex = annex;
    }

    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    /** 附件 */
    private String annex;

    private SysUser sysUser;

    @Override
    public String toString() {
        return "CarAndUserVO{" +
                "carId=" + carId +
                ", carName='" + carName + '\'' +
                ", color='" + color + '\'' +
                ", num='" + num + '\'' +
                ", status=" + status +
                ", userId=" + userId +
                ", driverId=" + driverId +
                ", annex='" + annex + '\'' +
                ", sysUser=" + sysUser +
                '}';
    }
}
