package com.ruoyi.common.core.domain.entity;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 车辆记录对象 car_record
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public class CarRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 记录编号 */
    @Excel(name = "记录编号")
    private Long recordId;

    /** 记录名称 */
    @Excel(name = "记录名称")
    private String recordName;

    /** 车辆编号 */
    @Excel(name = "车辆编号")
    private Long carId;

    /** 创建人 */
    @Excel(name = "创建人")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long departmentId;

    /** 开始时间 */
    private Date startTime;

    /** 结束时间 */
    private Date finishTime;

    /** 始发地 */
    private String birthland;

    /** 目的地 */
    private String destination;

    /** 用车事由 */
    private String reason;

    /** 附件 */
    private String annex;

    /** 操作名称 */
    @Excel(name = "操作名称")
    private Long operate;

    public void setRecordId(Long recordId) 
    {
        this.recordId = recordId;
    }

    public Long getRecordId() 
    {
        return recordId;
    }
    public void setRecordName(String recordName) 
    {
        this.recordName = recordName;
    }

    public String getRecordName() 
    {
        return recordName;
    }
    public void setCarId(Long carId) 
    {
        this.carId = carId;
    }

    public Long getCarId() 
    {
        return carId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setDepartmentId(Long departmentId) 
    {
        this.departmentId = departmentId;
    }

    public Long getDepartmentId() 
    {
        return departmentId;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setFinishTime(Date finishTime) 
    {
        this.finishTime = finishTime;
    }

    public Date getFinishTime() 
    {
        return finishTime;
    }
    public void setBirthland(String birthland) 
    {
        this.birthland = birthland;
    }

    public String getBirthland() 
    {
        return birthland;
    }
    public void setDestination(String destination) 
    {
        this.destination = destination;
    }

    public String getDestination() 
    {
        return destination;
    }
    public void setReason(String reason) 
    {
        this.reason = reason;
    }

    public String getReason() 
    {
        return reason;
    }
    public void setAnnex(String annex) 
    {
        this.annex = annex;
    }

    public String getAnnex() 
    {
        return annex;
    }
    public void setOperate(Long operate) 
    {
        this.operate = operate;
    }

    public Long getOperate() 
    {
        return operate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("recordId", getRecordId())
            .append("recordName", getRecordName())
            .append("carId", getCarId())
            .append("userId", getUserId())
            .append("departmentId", getDepartmentId())
            .append("createTime", getCreateTime())
            .append("startTime", getStartTime())
            .append("finishTime", getFinishTime())
            .append("birthland", getBirthland())
            .append("destination", getDestination())
            .append("reason", getReason())
            .append("remark", getRemark())
            .append("annex", getAnnex())
            .append("operate", getOperate())
            .toString();
    }
}
