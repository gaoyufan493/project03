package com.ruoyi.common.core.domain.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 车辆对象 car_detail
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
public class CarDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 车辆编号 */
    private Long carId;

    /** 车辆名称 */
    @Excel(name = "车辆名称")
    private String carName;

    /** 车辆颜色 */
    @Excel(name = "车辆颜色")
    private String color;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String num;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 创建人 */
    @Excel(name = "创建人")
    private Long userId;

    /** 司机_id */
    @Excel(name = "司机_id")
    private Long driverId;

    /** 附件 */
    @Excel(name = "附件")
    private String annex;

    public void setCarId(Long carId) 
    {
        this.carId = carId;
    }

    public Long getCarId() 
    {
        return carId;
    }
    public void setCarName(String carName) 
    {
        this.carName = carName;
    }

    public String getCarName() 
    {
        return carName;
    }
    public void setColor(String color) 
    {
        this.color = color;
    }

    public String getColor() 
    {
        return color;
    }
    public void setNum(String num) 
    {
        this.num = num;
    }

    public String getNum() 
    {
        return num;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setDriverId(Long driverId) 
    {
        this.driverId = driverId;
    }

    public Long getDriverId() 
    {
        return driverId;
    }
    public void setAnnex(String annex) 
    {
        this.annex = annex;
    }

    public String getAnnex() 
    {
        return annex;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("carId", getCarId())
            .append("carName", getCarName())
            .append("color", getColor())
            .append("num", getNum())
            .append("status", getStatus())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .append("driverId", getDriverId())
            .append("annex", getAnnex())
            .toString();
    }
}
