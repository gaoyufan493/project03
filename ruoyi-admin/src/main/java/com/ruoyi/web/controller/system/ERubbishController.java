package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ERubbish;
import com.ruoyi.system.service.IERubbishService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 垃圾Controller
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
@Controller
@RequestMapping("/system/rubbish")
public class ERubbishController extends BaseController
{
    private String prefix = "system/rubbish";

    @Autowired
    private IERubbishService eRubbishService;

    @RequiresPermissions("system:rubbish:view")
    @GetMapping()
    public String rubbish()
    {
        return prefix + "/rubbish";
    }

    /**
     * 查询垃圾列表
     */
    @RequiresPermissions("system:rubbish:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ERubbish eRubbish)
    {
        startPage();
        List<ERubbish> list = eRubbishService.selectERubbishList(eRubbish);
        return getDataTable(list);
    }

    /**
     * 导出垃圾列表
     */
    @RequiresPermissions("system:rubbish:export")
    @Log(title = "垃圾", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ERubbish eRubbish)
    {
        List<ERubbish> list = eRubbishService.selectERubbishList(eRubbish);
        ExcelUtil<ERubbish> util = new ExcelUtil<ERubbish>(ERubbish.class);
        return util.exportExcel(list, "垃圾数据");
    }

    /**
     * 新增垃圾
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存垃圾
     */
    @RequiresPermissions("system:rubbish:add")
    @Log(title = "垃圾", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ERubbish eRubbish)
    {
        return toAjax(eRubbishService.insertERubbish(eRubbish));
    }

    /**
     * 修改垃圾
     */
    @RequiresPermissions("system:rubbish:edit")
    @GetMapping("/edit/{rubbishId}")
    public String edit(@PathVariable("rubbishId") Long rubbishId, ModelMap mmap)
    {
        ERubbish eRubbish = eRubbishService.selectERubbishByRubbishId(rubbishId);
        mmap.put("eRubbish", eRubbish);
        return prefix + "/edit";
    }

    /**
     * 修改保存垃圾
     */
    @RequiresPermissions("system:rubbish:edit")
    @Log(title = "垃圾", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ERubbish eRubbish)
    {
        return toAjax(eRubbishService.updateERubbish(eRubbish));
    }

    /**
     * 删除垃圾
     */
    @RequiresPermissions("system:rubbish:remove")
    @Log(title = "垃圾", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(eRubbishService.deleteERubbishByRubbishIds(ids));
    }
}
