package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.domain.entity.CarRecord;
import com.ruoyi.system.service.ICarRecordService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 车辆记录Controller
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
@Controller
@RequestMapping("/system/record")
public class CarRecordController extends BaseController
{
    private String prefix = "system/record";

    @Autowired
    private ICarRecordService carRecordService;

    @RequiresPermissions("system:record:view")
    @GetMapping()
    public String record()
    {
        return prefix + "/record";
    }

    /**
     * 查询车辆记录列表
     */
    @RequiresPermissions("system:record:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarRecord carRecord)
    {
        startPage();
        List<CarRecord> list = carRecordService.selectCarRecordList(carRecord);
        return getDataTable(list);
    }

    /**
     * 导出车辆记录列表
     */
    @RequiresPermissions("system:record:export")
    @Log(title = "车辆记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarRecord carRecord)
    {
        List<CarRecord> list = carRecordService.selectCarRecordList(carRecord);
        ExcelUtil<CarRecord> util = new ExcelUtil<CarRecord>(CarRecord.class);
        return util.exportExcel(list, "车辆记录数据");
    }

    /**
     * 新增车辆记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存车辆记录
     */
    @RequiresPermissions("system:record:add")
    @Log(title = "车辆记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarRecord carRecord)
    {
        return toAjax(carRecordService.insertCarRecord(carRecord));
    }

    /**
     * 修改车辆记录
     */
    @RequiresPermissions("system:record:edit")
    @GetMapping("/edit/{recordId}")
    public String edit(@PathVariable("recordId") Long recordId, ModelMap mmap)
    {
        CarRecord carRecord = carRecordService.selectCarRecordByRecordId(recordId);
        mmap.put("carRecord", carRecord);
        return prefix + "/edit";
    }

    /**
     * 修改保存车辆记录
     */
    @RequiresPermissions("system:record:edit")
    @Log(title = "车辆记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarRecord carRecord)
    {
        return toAjax(carRecordService.updateCarRecord(carRecord));
    }

    /**
     * 删除车辆记录
     */
    @RequiresPermissions("system:record:remove")
    @Log(title = "车辆记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carRecordService.deleteCarRecordByRecordIds(ids));
    }
}
