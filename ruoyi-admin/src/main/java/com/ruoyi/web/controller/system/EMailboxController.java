package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.EMailbox;
import com.ruoyi.system.service.IEMailboxService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 邮箱Controller
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
@Controller
@RequestMapping("/system/mailbox")
public class EMailboxController extends BaseController
{
    private String prefix = "system/mailbox";

    @Autowired
    private IEMailboxService eMailboxService;

    @RequiresPermissions("system:mailbox:view")
    @GetMapping()
    public String mailbox()
    {
        return prefix + "/mailbox";
    }

    /**
     * 查询邮箱列表
     */
    @RequiresPermissions("system:mailbox:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(EMailbox eMailbox)
    {
        startPage();
        List<EMailbox> list = eMailboxService.selectEMailboxList(eMailbox);
        return getDataTable(list);
    }

    /**
     * 导出邮箱列表
     */
    @RequiresPermissions("system:mailbox:export")
    @Log(title = "邮箱", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(EMailbox eMailbox)
    {
        List<EMailbox> list = eMailboxService.selectEMailboxList(eMailbox);
        ExcelUtil<EMailbox> util = new ExcelUtil<EMailbox>(EMailbox.class);
        return util.exportExcel(list, "邮箱数据");
    }

    /**
     * 新增邮箱
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存邮箱
     */
    @RequiresPermissions("system:mailbox:add")
    @Log(title = "邮箱", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(EMailbox eMailbox)
    {
        return toAjax(eMailboxService.insertEMailbox(eMailbox));
    }

    /**
     * 修改邮箱
     */
    @RequiresPermissions("system:mailbox:edit")
    @GetMapping("/edit/{emailId}")
    public String edit(@PathVariable("emailId") Long emailId, ModelMap mmap)
    {
        EMailbox eMailbox = eMailboxService.selectEMailboxByEmailId(emailId);
        mmap.put("eMailbox", eMailbox);
        return prefix + "/edit";
    }

    /**
     * 修改保存邮箱
     */
    @RequiresPermissions("system:mailbox:edit")
    @Log(title = "邮箱", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(EMailbox eMailbox)
    {
        return toAjax(eMailboxService.updateEMailbox(eMailbox));
    }

    /**
     * 删除邮箱
     */
    @RequiresPermissions("system:mailbox:remove")
    @Log(title = "邮箱", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(eMailboxService.deleteEMailboxByEmailIds(ids));
    }
}
