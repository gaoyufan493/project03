package com.ruoyi.web.controller.system;

import java.util.Date;
import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ShiroUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.domain.entity.CarDetail;
import com.ruoyi.system.service.ICarDetailService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 车辆Controller
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
@Controller
@RequestMapping("/system/detail")
public class CarDetailController extends BaseController
{
    private String prefix = "system/detail";

    @Autowired
    private ICarDetailService carDetailService;

    @RequiresPermissions("system:detail:view")
    @GetMapping()
    public String detail()
    {
        return prefix + "/detail";
    }

    /**
     * 查询车辆列表
     */
    @RequiresPermissions("system:detail:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarDetail carDetail)
    {
        startPage();
        List<CarDetail> list = carDetailService.selectCarDetailList(carDetail);
        return getDataTable(list);
    }
    public String toView(){
        return prefix + "/view";
    }
    /**
     * 导出车辆列表
     */
    @RequiresPermissions("system:detail:export")
    @Log(title = "车辆", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarDetail carDetail)
    {
        List<CarDetail> list = carDetailService.selectCarDetailList(carDetail);
        ExcelUtil<CarDetail> util = new ExcelUtil<CarDetail>(CarDetail.class);
        return util.exportExcel(list, "车辆数据");
    }

    /**
     * 新增车辆
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存车辆
     */
    @RequiresPermissions("system:detail:add")
    @Log(title = "车辆", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarDetail carDetail)

    {
//        SysUser currentUser = ShiroUtils.getSysUser();
//        String userName = currentUser .getUserName();
//        vehicleList.setCreatedBy(userName);
//        Date nowDate = DateUtils.getNowDate();
//        vehicleList.setCreatedAt(nowDate);
        return toAjax(carDetailService.insertCarDetail(carDetail));
    }

    /**
     * 修改车辆
     */
    @RequiresPermissions("system:detail:edit")
    @GetMapping("/edit/{carId}")
    public String edit(@PathVariable("carId") Long carId, ModelMap mmap)
    {
        CarDetail carDetail = carDetailService.selectCarDetailByCarId(carId);
        mmap.put("carDetail", carDetail);
        return prefix + "/edit";
    }

    /**
     * 修改保存车辆
     */
    @RequiresPermissions("system:detail:edit")
    @Log(title = "车辆", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarDetail carDetail)
    {
        return toAjax(carDetailService.updateCarDetail(carDetail));
    }

    /**
     * 删除车辆
     */
    @RequiresPermissions("system:detail:remove")
    @Log(title = "车辆", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carDetailService.deleteCarDetailByCarIds(ids));
    }
}
