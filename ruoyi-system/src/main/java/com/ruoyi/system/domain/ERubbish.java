package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 垃圾对象 e_rubbish
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
public class ERubbish extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 垃圾编号 */
    @Excel(name = "垃圾编号")
    private Long rubbishId;

    /** 邮件编号 */
    @Excel(name = "邮件编号")
    private Long emailId;

    /** 用户编号 */
    @Excel(name = "用户编号")
    private Long userId;

    public void setRubbishId(Long rubbishId) 
    {
        this.rubbishId = rubbishId;
    }

    public Long getRubbishId() 
    {
        return rubbishId;
    }
    public void setEmailId(Long emailId) 
    {
        this.emailId = emailId;
    }

    public Long getEmailId() 
    {
        return emailId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rubbishId", getRubbishId())
            .append("emailId", getEmailId())
            .append("userId", getUserId())
            .toString();
    }
}
