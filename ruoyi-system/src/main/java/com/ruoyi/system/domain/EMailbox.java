package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 邮箱对象 e_mailbox
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
public class EMailbox extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 邮箱编号 */
    private Long emailId;

    /** 邮件名称 */
    @Excel(name = "邮件名称")
    private String emailName;

    /** 邮件类型(0=机密文件,1=普通文件,2=其他) */
    @Excel(name = "邮件类型(0=机密文件,1=普通文件,2=其他)")
    private Long type;

    /** 发件人编号 */
    @Excel(name = "发件人编号")
    private Long senderId;

    /** 收件人编号 */
    @Excel(name = "收件人编号")
    private Long recipientId;

    /** 发件时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发件时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date sendTime;

    /** 状态(0=未读,1=已读) */
    @Excel(name = "状态(0=未读,1=已读)")
    private Long status;

    /** 邮件内容 */
    @Excel(name = "邮件内容")
    private String content;

    /** 附件 */
    @Excel(name = "附件")
    private String annex;

    public void setEmailId(Long emailId) 
    {
        this.emailId = emailId;
    }

    public Long getEmailId() 
    {
        return emailId;
    }
    public void setEmailName(String emailName) 
    {
        this.emailName = emailName;
    }

    public String getEmailName() 
    {
        return emailName;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setSenderId(Long senderId) 
    {
        this.senderId = senderId;
    }

    public Long getSenderId() 
    {
        return senderId;
    }
    public void setRecipientId(Long recipientId) 
    {
        this.recipientId = recipientId;
    }

    public Long getRecipientId() 
    {
        return recipientId;
    }
    public void setSendTime(Date sendTime) 
    {
        this.sendTime = sendTime;
    }

    public Date getSendTime() 
    {
        return sendTime;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setAnnex(String annex) 
    {
        this.annex = annex;
    }

    public String getAnnex() 
    {
        return annex;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("emailId", getEmailId())
            .append("emailName", getEmailName())
            .append("type", getType())
            .append("senderId", getSenderId())
            .append("recipientId", getRecipientId())
            .append("sendTime", getSendTime())
            .append("status", getStatus())
            .append("content", getContent())
            .append("createTime", getCreateTime())
            .append("annex", getAnnex())
            .toString();
    }
}
