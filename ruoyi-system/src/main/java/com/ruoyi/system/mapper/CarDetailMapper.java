package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.common.core.domain.entity.CarDetail;

/**
 * 车辆Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
public interface CarDetailMapper 
{
    /**
     * 查询车辆
     * 
     * @param carId 车辆主键
     * @return 车辆
     */
    public CarDetail selectCarDetailByCarId(Long carId);

    /**
     * 查询车辆列表
     * 
     * @param carDetail 车辆
     * @return 车辆集合
     */
    public List<CarDetail> selectCarDetailList(CarDetail carDetail);

    /**
     * 新增车辆
     * 
     * @param carDetail 车辆
     * @return 结果
     */
    public int insertCarDetail(CarDetail carDetail);

    /**
     * 修改车辆
     * 
     * @param carDetail 车辆
     * @return 结果
     */
    public int updateCarDetail(CarDetail carDetail);

    /**
     * 删除车辆
     * 
     * @param carId 车辆主键
     * @return 结果
     */
    public int deleteCarDetailByCarId(Long carId);

    /**
     * 批量删除车辆
     * 
     * @param carIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarDetailByCarIds(String[] carIds);
}
