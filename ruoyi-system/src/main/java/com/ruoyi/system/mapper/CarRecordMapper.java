package com.ruoyi.system.mapper;


import com.ruoyi.common.core.domain.entity.CarRecord;

import java.util.List;

/**
 * 车辆记录Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public interface CarRecordMapper 
{
    /**
     * 查询车辆记录
     * 
     * @param recordId 车辆记录主键
     * @return 车辆记录
     */
    public CarRecord selectCarRecordByRecordId(Long recordId);

    /**
     * 查询车辆记录列表
     * 
     * @param carRecord 车辆记录
     * @return 车辆记录集合
     */
    public List<CarRecord> selectCarRecordList(CarRecord carRecord);

    /**
     * 新增车辆记录
     * 
     * @param carRecord 车辆记录
     * @return 结果
     */
    public int insertCarRecord(CarRecord carRecord);

    /**
     * 修改车辆记录
     * 
     * @param carRecord 车辆记录
     * @return 结果
     */
    public int updateCarRecord(CarRecord carRecord);

    /**
     * 删除车辆记录
     * 
     * @param recordId 车辆记录主键
     * @return 结果
     */
    public int deleteCarRecordByRecordId(Long recordId);

    /**
     * 批量删除车辆记录
     * 
     * @param recordIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarRecordByRecordIds(String[] recordIds);
}
