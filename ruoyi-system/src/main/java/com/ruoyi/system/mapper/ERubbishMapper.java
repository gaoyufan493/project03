package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ERubbish;

/**
 * 垃圾Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
public interface ERubbishMapper 
{
    /**
     * 查询垃圾
     * 
     * @param rubbishId 垃圾主键
     * @return 垃圾
     */
    public ERubbish selectERubbishByRubbishId(Long rubbishId);

    /**
     * 查询垃圾列表
     * 
     * @param eRubbish 垃圾
     * @return 垃圾集合
     */
    public List<ERubbish> selectERubbishList(ERubbish eRubbish);

    /**
     * 新增垃圾
     * 
     * @param eRubbish 垃圾
     * @return 结果
     */
    public int insertERubbish(ERubbish eRubbish);

    /**
     * 修改垃圾
     * 
     * @param eRubbish 垃圾
     * @return 结果
     */
    public int updateERubbish(ERubbish eRubbish);

    /**
     * 删除垃圾
     * 
     * @param rubbishId 垃圾主键
     * @return 结果
     */
    public int deleteERubbishByRubbishId(Long rubbishId);

    /**
     * 批量删除垃圾
     * 
     * @param rubbishIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteERubbishByRubbishIds(String[] rubbishIds);
}
