package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.EMailbox;

/**
 * 邮箱Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
public interface EMailboxMapper 
{
    /**
     * 查询邮箱
     * 
     * @param emailId 邮箱主键
     * @return 邮箱
     */
    public EMailbox selectEMailboxByEmailId(Long emailId);

    /**
     * 查询邮箱列表
     * 
     * @param eMailbox 邮箱
     * @return 邮箱集合
     */
    public List<EMailbox> selectEMailboxList(EMailbox eMailbox);

    /**
     * 新增邮箱
     * 
     * @param eMailbox 邮箱
     * @return 结果
     */
    public int insertEMailbox(EMailbox eMailbox);

    /**
     * 修改邮箱
     * 
     * @param eMailbox 邮箱
     * @return 结果
     */
    public int updateEMailbox(EMailbox eMailbox);

    /**
     * 删除邮箱
     * 
     * @param emailId 邮箱主键
     * @return 结果
     */
    public int deleteEMailboxByEmailId(Long emailId);

    /**
     * 批量删除邮箱
     * 
     * @param emailIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEMailboxByEmailIds(String[] emailIds);
}
