package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.common.core.domain.entity.CarDetail;

/**
 * 车辆Service接口
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
public interface ICarDetailService 
{
    /**
     * 查询车辆
     * 
     * @param carId 车辆主键
     * @return 车辆
     */
    public CarDetail selectCarDetailByCarId(Long carId);

    /**
     * 查询车辆列表
     * 
     * @param carDetail 车辆
     * @return 车辆集合
     */
    public List<CarDetail> selectCarDetailList(CarDetail carDetail);

    /**
     * 新增车辆
     * 
     * @param carDetail 车辆
     * @return 结果
     */
    public int insertCarDetail(CarDetail carDetail);

    /**
     * 修改车辆
     * 
     * @param carDetail 车辆
     * @return 结果
     */
    public int updateCarDetail(CarDetail carDetail);

    /**
     * 批量删除车辆
     * 
     * @param carIds 需要删除的车辆主键集合
     * @return 结果
     */
    public int deleteCarDetailByCarIds(String carIds);

    /**
     * 删除车辆信息
     * 
     * @param carId 车辆主键
     * @return 结果
     */
    public int deleteCarDetailByCarId(Long carId);
}
