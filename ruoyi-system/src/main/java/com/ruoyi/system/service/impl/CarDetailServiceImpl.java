package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CarDetailMapper;
import com.ruoyi.common.core.domain.entity.CarDetail;
import com.ruoyi.system.service.ICarDetailService;
import com.ruoyi.common.core.text.Convert;

/**
 * 车辆Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
@Service
public class CarDetailServiceImpl implements ICarDetailService 
{
    @Autowired
    private CarDetailMapper carDetailMapper;

    /**
     * 查询车辆
     * 
     * @param carId 车辆主键
     * @return 车辆
     */
    @Override
    public CarDetail selectCarDetailByCarId(Long carId)
    {
        return carDetailMapper.selectCarDetailByCarId(carId);
    }

    /**
     * 查询车辆列表
     * 
     * @param carDetail 车辆
     * @return 车辆
     */
    @Override
    public List<CarDetail> selectCarDetailList(CarDetail carDetail)
    {
        return carDetailMapper.selectCarDetailList(carDetail);
    }

    /**
     * 新增车辆
     * 
     * @param carDetail 车辆
     * @return 结果
     */
    @Override
    public int insertCarDetail(CarDetail carDetail)
    {
        carDetail.setCreateTime(DateUtils.getNowDate());
        return carDetailMapper.insertCarDetail(carDetail);
    }

    /**
     * 修改车辆
     * 
     * @param carDetail 车辆
     * @return 结果
     */
    @Override
    public int updateCarDetail(CarDetail carDetail)
    {
        return carDetailMapper.updateCarDetail(carDetail);
    }

    /**
     * 批量删除车辆
     * 
     * @param carIds 需要删除的车辆主键
     * @return 结果
     */
    @Override
    public int deleteCarDetailByCarIds(String carIds)
    {
        return carDetailMapper.deleteCarDetailByCarIds(Convert.toStrArray(carIds));
    }

    /**
     * 删除车辆信息
     * 
     * @param carId 车辆主键
     * @return 结果
     */
    @Override
    public int deleteCarDetailByCarId(Long carId)
    {
        return carDetailMapper.deleteCarDetailByCarId(carId);
    }
}
