package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.EMailboxMapper;
import com.ruoyi.system.domain.EMailbox;
import com.ruoyi.system.service.IEMailboxService;
import com.ruoyi.common.core.text.Convert;

/**
 * 邮箱Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
@Service
public class EMailboxServiceImpl implements IEMailboxService 
{
    @Autowired
    private EMailboxMapper eMailboxMapper;

    /**
     * 查询邮箱
     * 
     * @param emailId 邮箱主键
     * @return 邮箱
     */
    @Override
    public EMailbox selectEMailboxByEmailId(Long emailId)
    {
        return eMailboxMapper.selectEMailboxByEmailId(emailId);
    }

    /**
     * 查询邮箱列表
     * 
     * @param eMailbox 邮箱
     * @return 邮箱
     */
    @Override
    public List<EMailbox> selectEMailboxList(EMailbox eMailbox)
    {
        return eMailboxMapper.selectEMailboxList(eMailbox);
    }

    /**
     * 新增邮箱
     * 
     * @param eMailbox 邮箱
     * @return 结果
     */
    @Override
    public int insertEMailbox(EMailbox eMailbox)
    {
        eMailbox.setCreateTime(DateUtils.getNowDate());
        return eMailboxMapper.insertEMailbox(eMailbox);
    }

    /**
     * 修改邮箱
     * 
     * @param eMailbox 邮箱
     * @return 结果
     */
    @Override
    public int updateEMailbox(EMailbox eMailbox)
    {
        return eMailboxMapper.updateEMailbox(eMailbox);
    }

    /**
     * 批量删除邮箱
     * 
     * @param emailIds 需要删除的邮箱主键
     * @return 结果
     */
    @Override
    public int deleteEMailboxByEmailIds(String emailIds)
    {
        return eMailboxMapper.deleteEMailboxByEmailIds(Convert.toStrArray(emailIds));
    }

    /**
     * 删除邮箱信息
     * 
     * @param emailId 邮箱主键
     * @return 结果
     */
    @Override
    public int deleteEMailboxByEmailId(Long emailId)
    {
        return eMailboxMapper.deleteEMailboxByEmailId(emailId);
    }
}
