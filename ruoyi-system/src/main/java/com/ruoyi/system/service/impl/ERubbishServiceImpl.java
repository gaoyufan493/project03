package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ERubbishMapper;
import com.ruoyi.system.domain.ERubbish;
import com.ruoyi.system.service.IERubbishService;
import com.ruoyi.common.core.text.Convert;

/**
 * 垃圾Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
@Service
public class ERubbishServiceImpl implements IERubbishService 
{
    @Autowired
    private ERubbishMapper eRubbishMapper;

    /**
     * 查询垃圾
     * 
     * @param rubbishId 垃圾主键
     * @return 垃圾
     */
    @Override
    public ERubbish selectERubbishByRubbishId(Long rubbishId)
    {
        return eRubbishMapper.selectERubbishByRubbishId(rubbishId);
    }

    /**
     * 查询垃圾列表
     * 
     * @param eRubbish 垃圾
     * @return 垃圾
     */
    @Override
    public List<ERubbish> selectERubbishList(ERubbish eRubbish)
    {
        return eRubbishMapper.selectERubbishList(eRubbish);
    }

    /**
     * 新增垃圾
     * 
     * @param eRubbish 垃圾
     * @return 结果
     */
    @Override
    public int insertERubbish(ERubbish eRubbish)
    {
        return eRubbishMapper.insertERubbish(eRubbish);
    }

    /**
     * 修改垃圾
     * 
     * @param eRubbish 垃圾
     * @return 结果
     */
    @Override
    public int updateERubbish(ERubbish eRubbish)
    {
        return eRubbishMapper.updateERubbish(eRubbish);
    }

    /**
     * 批量删除垃圾
     * 
     * @param rubbishIds 需要删除的垃圾主键
     * @return 结果
     */
    @Override
    public int deleteERubbishByRubbishIds(String rubbishIds)
    {
        return eRubbishMapper.deleteERubbishByRubbishIds(Convert.toStrArray(rubbishIds));
    }

    /**
     * 删除垃圾信息
     * 
     * @param rubbishId 垃圾主键
     * @return 结果
     */
    @Override
    public int deleteERubbishByRubbishId(Long rubbishId)
    {
        return eRubbishMapper.deleteERubbishByRubbishId(rubbishId);
    }
}
