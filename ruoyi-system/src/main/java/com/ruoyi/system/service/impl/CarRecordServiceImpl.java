package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.entity.CarRecord;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.mapper.CarRecordMapper;
import com.ruoyi.system.service.ICarRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 车辆记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
@Service
public class CarRecordServiceImpl implements ICarRecordService 
{
    @Autowired
    private CarRecordMapper carRecordMapper;

    /**
     * 查询车辆记录
     * 
     * @param recordId 车辆记录主键
     * @return 车辆记录
     */
    @Override
    public CarRecord selectCarRecordByRecordId(Long recordId)
    {
        return carRecordMapper.selectCarRecordByRecordId(recordId);
    }

    /**
     * 查询车辆记录列表
     * 
     * @param carRecord 车辆记录
     * @return 车辆记录
     */
    @Override
    public List<CarRecord> selectCarRecordList(CarRecord carRecord)
    {
        return carRecordMapper.selectCarRecordList(carRecord);
    }

    /**
     * 新增车辆记录
     * 
     * @param carRecord 车辆记录
     * @return 结果
     */
    @Override
    public int insertCarRecord(CarRecord carRecord)
    {
        carRecord.setCreateTime(DateUtils.getNowDate());
        return carRecordMapper.insertCarRecord(carRecord);
    }

    /**
     * 修改车辆记录
     * 
     * @param carRecord 车辆记录
     * @return 结果
     */
    @Override
    public int updateCarRecord(CarRecord carRecord)
    {
        return carRecordMapper.updateCarRecord(carRecord);
    }

    /**
     * 批量删除车辆记录
     * 
     * @param recordIds 需要删除的车辆记录主键
     * @return 结果
     */
    @Override
    public int deleteCarRecordByRecordIds(String recordIds)
    {
        return carRecordMapper.deleteCarRecordByRecordIds(Convert.toStrArray(recordIds));
    }

    /**
     * 删除车辆记录信息
     * 
     * @param recordId 车辆记录主键
     * @return 结果
     */
    @Override
    public int deleteCarRecordByRecordId(Long recordId)
    {
        return carRecordMapper.deleteCarRecordByRecordId(recordId);
    }
}
